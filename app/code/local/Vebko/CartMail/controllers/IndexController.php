<?php
class Vebko_CartMail_IndexController extends Mage_Core_Controller_Front_Action {
    public function indexAction() {
        $this->loadLayout(array('default'));
        $this->renderLayout();
    }

    public function SendMailAction() {
        $adminMail = 'contact@trgovski.com'; // send to email
        $adminSubject = $this->__('New order from ozone.com.al'); // Title of email
		$adminMessage = '';
		$customerSubject = $this->__('New order from ozone.com.al'); // Title of email
		$customerMessage = '';
		$headers = "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";	
        
        $vebkofistname      =    Mage::app()->getRequest()->getParam('vebkofistname');
        $vebkolastname      =    Mage::app()->getRequest()->getParam('vebkolastname');
        $vebkostreet        =    Mage::app()->getRequest()->getParam('vebkostreet');
        $vebkostreetnumber  =    Mage::app()->getRequest()->getParam('vebkostreetnumber');
        $vebkocity          =    Mage::app()->getRequest()->getParam('vebkocity');
        $vebkocountry       =    Mage::app()->getRequest()->getParam('vebkocountry');
        $vebkoemail         =    Mage::app()->getRequest()->getParam('vebkoemail');
        $vebkotelefon       =    Mage::app()->getRequest()->getParam('vebkotelefon');
        
		$customerMessage .= '<div style="width: 100%; max-width: 800px; height: 600px; margin: 0 auto; background-color: #E8E8E8; border: 0px solid black;">
								<div style="width: 100%; height: 120px;">
									<div style="width: 48%; display: inline; float: left; margin: 1%;">
										<img src="http://demo.ozone.com.al/skin/frontend/default/grayscale2014/images/ozonelogo.png">
									</div>
									<div style="width: 48%; display: inline; float: left; text-align: right; margin: 6px;">
										'.$this->__('Ozone Electronics').'<br>
										'.$this->__('Address: Mail Address').'<br>
										'.$this->__('Phone: xxxxx').'<br>
									</div>
								</div>
								<div style="width: 100%;">
									<div style="width: 98%;margin: 1%;">
										'.$this->__('Dear').' '.$vebkofistname.'! <br><br>
										'.$this->__('Thank you for order for our store.'). '<br>
										'.$this->__('Your order list is follow:'). '<br><br>
									</div>
									<div style="width: 100%;margin: 1%;">
										<table style="width:98%">
											<tr style="background-color: #D0D0D0;">
												<td>'.$this->__('Product Name').'</td>
												<td>'.$this->__('Unit Price').'</td>
												<td>'.$this->__('Qty').'</td>
												<td style="text-align: right; padding: 2px;">'.$this->__('Subtotal').'</td>
											</tr>';	
        
        $adminMessage .= '<b>'.$this->__('List of Products').'</b><br>';

        $quote = Mage::getModel('checkout/session')->getQuote();
        $cartGrossTotal = 0;
        foreach ($quote->getAllItems() as $item) {
            $productName = $item->getProduct()->getName();
            $productPrice = $item->getProduct()->getFinalPrice();
            $productName = $item->getProduct()->getName();
            $productQty = $item->getQty();
            $cartGrossTotal += $item->getPriceInclTax() * $item->getQty();

            $adminMessage .= $productName . ':  ' . $productPrice . ' x ' . $productQty . ' = ' . $productPrice * $productQty . '<br>';
			$customerMessage .= '<tr style="">
									<td>'.$productName.'</td>
									<td>'.$productPrice.'</td>
									<td>'.$productQty.'</td>
									<td style="text-align: right; padding: 2px;">'.$productPrice * $productQty.' '.$this->__('vebko_all').'</td>
								</tr>';
        }
        $adminMessage .='<b>'.$this->__('Total:').' '.$cartGrossTotal.' '.$this->__('vebko_all').'<br><br><br>';
        
        $adminMessage .='<b>Customer Information:</b><br>';
        $adminMessage .=''. $vebkofistname . '  ' . $vebkolastname.'<br>';
        $adminMessage .=''.$vebkostreet.'  ' . $vebkostreetnumber.'<br>';
        $adminMessage .=''. $vebkocity.'  '.$vebkocountry.'<br>';
        $adminMessage .=''. $vebkotelefon.'<br>';
        $adminMessage .='' . $vebkoemail.'<br>';
		
		$customerMessage .= '<tr style="">
                    <td></td>
                    <td></td>
                    <td style="text-align: right; font-size: 20px; border-top: 1px solid #D0D0D0;">'.$this->__('Total:').'</td>
                    <td style="text-align: right; font-size: 20px; color: red; border-top: 1px solid #D0D0D0;">'.$cartGrossTotal.' '.$this->__('vebko_all').'</td>
                </tr>  
				</table> 
        </div>
        <div style="width: 98%;margin: 1%; margin-top: 40px;">
            '.$this->__('For any information please feel free to contact us over email or phone').'
            <br><br>
            '.$this->__('Best Regards,').'<br>
            '.$this->__('Ozone Electronics').'
        </div>
    </div>
</div>';

		
// Send to admin
        mail($adminMail, $adminSubject , $adminMessage, $headers);
// Send to user
		mail($vebkoemail, $customerSubject , $customerMessage, $headers);

        Mage::getSingleton('core/session')->addSuccess($this->__('Your order is send to ozone store, please check you are email: ').$vebkoemail);
		Mage::getSingleton('checkout/cart')->truncate();
		Mage::getSingleton('checkout/session')->clear();
        return $this->_redirect('');
		
    }
	
	

}